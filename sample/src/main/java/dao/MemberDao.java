package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.UserBean;

public class MemberDao {

	private Connection con = null; // コネクションオブジェクト
	private Statement stmt = null; // ステートメントオブジェクト
	private ConnectionManager cm; // コネクションマネージャー

	// Connectionの取得
	private void getConnection() throws DAOException {
		if (this.con != null) {
			return;
		}
		cm = ConnectionManager.getInstance();
		con = cm.getConnection(); // データベースへの接続の取得
	}

	// Statementの取得
	private void createStmt() throws DAOException {
		if (this.stmt != null) {
			return;
		}
		try {
			stmt = con.createStatement();
		} catch (SQLException e) { // SQLに関する例外処理
			throw new DAOException("[createStmt]異常", e);
		}
	}

	// データを追加するメソッド
	public int insertMember(UserBean userbean) throws DAOException {
		getConnection();
		int count = 0;
		String sql = "INSERT INTO member (name, country, mail) VALUES( ?, ?, ?)";
		String name = userbean.getName();
		String country = userbean.getCountry();
		String mail = userbean.getMail();

		try (
			// INSERT文の準備
			PreparedStatement pstmt = con.prepareStatement(sql)) {
			pstmt.setString(1, name);
			pstmt.setString(2, country);
			pstmt.setString(3, mail);
			// INSERT文を実行
			count += pstmt.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("[UserDAO#insertMember]異常", e);
		} finally {
			close();
		}
		return count;
	}

	// データを読み取るメソッド
	public ArrayList<UserBean> findAll() throws DAOException {
		// 検索結果をリターンするためのリスト
		ArrayList<UserBean> MemberList = new ArrayList<UserBean>();
		String sql = "SELECT * from member";
		getConnection();

		try {
			// SELECT文の準備
			PreparedStatement pStmt = con.prepareStatement(sql);
			ResultSet rs = pStmt.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String country = rs.getString("country");
				String mail = rs.getString("mail");
				UserBean userbean = new UserBean(id, name, country, mail);
				MemberList.add(userbean);
			}
		} catch (SQLException e) {
			throw new DAOException("[memberDAO#findAll]異常", e);
		} finally {
			close();
		}
		return MemberList;
	}

	// データを1件取得し、更新するメソッド
	public int upDetaMember(UserBean userbean) throws DAOException {
		String sql = "UPDATE member SET name=?,country=?,mail=? WHERE id=?";
		getConnection();
		int count = 0;
		int id = userbean.getId();
		String name = userbean.getName();
		String country = userbean.getCountry();
		String mail = userbean.getMail();
		try {
			// UPDATE文の準備
			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, name);
			pStmt.setString(2, country);
			pStmt.setString(3, mail);
			pStmt.setInt(4, id);
			// UPDATE文を実行
			count += pStmt.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("[memberDAO#upDetaMembe]異常", e);
		} finally {
			close();
		}
		return count;
	}

	// データを1件取得し、削除するメソッド
	public int deleteMember(UserBean userbean) throws DAOException {
		String sql = "DELETE from member WHERE id=?";
		getConnection();
		int count = 0;
		int id = userbean.getId();
		try {
			// DELETE文の準備
			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setInt(1, id);
			// DELETE文を実行
			count += pStmt.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("[memberDAO#deleteMember]異常", e);
		} finally {
			close();
		}
		return count;
	}

	private void close() throws DAOException {
		try {
			if (stmt != null) {
				stmt.close();
			}
		} catch (SQLException e) {
			throw new DAOException("[closeStatement]異常", e);
		} finally {
			this.stmt = null;
			this.cm = null;
		}
	}
}