package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.SearchBean;
import model.UserBean;


public class SearchDao {

	private Connection con = null; // コネクションオブジェクト
	private Statement stmt = null; // ステートメントオブジェクト
	private ConnectionManager cm; // コネクションマネージャー

	// Connectionの取得
	private void getConnection() throws DAOException {
		if (this.con != null) {
			return;
		}
		cm = ConnectionManager.getInstance();
		con = cm.getConnection(); // データベースへの接続の取得
	}

	// Statementの取得
	private void createStmt() throws DAOException {
		if (this.stmt != null) {
			return;
		}
		try {
			stmt = con.createStatement();
		} catch (SQLException e) { // SQLに関する例外処理
			throw new DAOException("[createStmt]異常", e);
		}
	}

	// 複数条件で検索するメソッド
	public List<UserBean> SearchMember(SearchBean searchBean) throws DAOException {
		// 検索結果をリターンするためのリスト
		ArrayList<UserBean> MemberList = new ArrayList<UserBean>();
		String sql = "SELECT * FROM member";
		try {
			int japanese = searchBean.getJapanese();
			String searchWord = searchBean.getSearchWord();
			int order = searchBean.getOrder();

			// 条件式の一部を準備
			boolean japanese0 = true;
			boolean japanese1 = false;
			boolean order0 = true;
			boolean order1 = false;
//			searchWord = "田中";
			
			// 日本人のみにチェックがない場合
			if (japanese == 0) {
				japanese0 = true;
			}
			// 日本人のみにチェックがある場合
			else if (japanese == 1) {
				japanese1 = false;
			}
			// 新しい順が選択された場合
			if (order == 0) {
				order0 = true;
			}
			// 古い順が選択された場合
			else if (order == 1) {
				order1 = false;
			}
			// 条件によりSQL書き換え
			// 検索文字列が空で、全員を新しい順に検索
			if (searchWord.isEmpty() && japanese0 && order0) {
				sql += "";
			}
			// 検索文字列が入力されて、全員を新しい順に検索
			else if (!searchWord.isEmpty() && japanese0 && order0) {
				// 検索条件のSQLを結合
				sql += " WHERE name LIKE '?%'";
			}
			// 検索文字列が空で、日本人を新しい順に検索
			else if (searchWord.isEmpty() && japanese1 && order0) {
				// 検索条件のSQLを結合
				sql += " WHERE country='日本'";
			}
			// 検索文字列が入力されて、日本人を新しい順に検索
			else if (!searchWord.isEmpty() && japanese1 && order0) {
				// 検索条件のSQLを結合
				sql += " WHERE country='日本' name LIKE '?%'";
			}
			// 検索文字列が空で、全員を古い順に検索
			else if (searchWord.isEmpty() && japanese1 && order1) {
				// 検索条件のSQLを結合
				sql += " ORDER BY id DESC";
			}
			// 検索文字列が入力されて、全員を古い順に検索
			else if (!searchWord.isEmpty() && japanese0 && order1) {
				// 検索条件のSQLを結合
				sql += " WHERE name LIKE '?%' ORDER BY id DESC";
			}
			// 検索文字列が空で、日本人を古い順に検索
			else if (searchWord.isEmpty() && japanese1 && order1) {
				// 検索条件のSQLを結合
				sql += " WHERE country='日本'  ORDER BY id DESC";
			}
			// 検索文字列が入力されて、日本人を古い順に検索
			else if (!searchWord.isEmpty() && japanese1 && order1) {
				// 検索条件のSQLを結合
				sql += " WHERE country='日本' name LIKE '?%' ORDER BY id DESC";
			}
			getConnection();
			PreparedStatement pStmt = con.prepareStatement(sql);
			ResultSet rs = pStmt.executeQuery();
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String country = rs.getString("country");
				String mail = rs.getString("mail");
				UserBean userbean = new UserBean(id, name, country, mail);
				MemberList.add(userbean);
			}
		} catch (SQLException e) {
			throw new DAOException("[memberDAO#SeachMember]異常", e);
		} finally {
			close();
		}
		return MemberList;
	}

	private void close() throws DAOException {
		try {
			if (stmt != null) {
				stmt.close();
			}
		} catch (SQLException e) {
			throw new DAOException("[closeStatement]異常", e);
		} finally {
			this.stmt = null;
			this.cm = null;
		}
	}

}