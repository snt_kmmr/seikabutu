package model;

import dao.DAOException;
import dao.MemberDao;

public class DeleteUserLogic {

	MemberDao memberDao = new MemberDao();

	public boolean execute(UserBean userbean) {
		boolean deleteCheck = false;
		// 登録処理
		try {
			memberDao.deleteMember(userbean);
			deleteCheck = true;
		} catch (DAOException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		return deleteCheck;
	}

}
