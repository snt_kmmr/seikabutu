package model;

import java.util.List;

import dao.DAOException;
import dao.MemberDao;

public class GetMemberListLogic {
	
    public List<UserBean> execute(){

    	MemberDao mdao = new MemberDao();
        List<UserBean> memberList = null;
		try {
			memberList = mdao.findAll();
		} catch (DAOException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
        return memberList;
    }
}
