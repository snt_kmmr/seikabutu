package model;
 
import dao.DAOException;
import dao.MemberDao;
 
public class RegisterUserLogic {
  MemberDao memberDao = new MemberDao();
    public boolean execute(UserBean userbean){
        boolean registCheck = false;
        //登録処理
        try {
            memberDao.insertMember(userbean);
            registCheck = true;
        } catch (DAOException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        }
        return registCheck;
    }
}