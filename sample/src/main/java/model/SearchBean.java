package model;
 
import java.io.Serializable;
 
public class SearchBean implements Serializable {
 
    //フィールド
	private int japanese;
    private String searchWord;
    private int order;

    
    public SearchBean(){}
    public SearchBean( int japanese ,String  searchWord ,int order){
    	this.japanese = japanese;
        this.searchWord = searchWord;
        this.order = order;
    }
    
	public int getJapanese() {
		return japanese;
	}
	public void setJapanese(int japanese) {
		this.japanese = japanese;
	}
	public String getSearchWord() {
		return searchWord;
	}
	public void setSearchWord(String searchWord) {
		this.searchWord = searchWord;
	}
	public int getOrder() {
		return order;
	}
	public void setOrder(int order) {
		this.order = order;
	}
   

}