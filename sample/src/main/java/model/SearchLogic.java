package model;

import java.util.List;

import dao.DAOException;
import dao.SearchDao;

public class SearchLogic {
	
    public static List<UserBean> execute(SearchBean SeachUser) {
    	
    	SearchDao sdao = new SearchDao();
    	List<UserBean> MemberList = null;

		try {
			MemberList = sdao.SearchMember(SeachUser);

		} catch (DAOException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		return MemberList;
    }
    
}
