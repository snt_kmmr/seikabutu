package model;
 
import java.io.Serializable;
 
public class UserBean implements Serializable {
 
    //フィールド
	private int id;
    private String name;
    private String country;
    private String mail;

    
    public UserBean(){}
    public UserBean( int id ,String name ,String  country ,String  mail){
    	this.id = id;
        this.name = name;
        this.country = country;
        this.mail = mail;
    }
    
	public int getId() {
		return id;
	}
    public String getName() {
        return name;
    }
    public String getCountry() {
        return country;
    }
    public String getMail() {
        return mail;
    }
	public void setName(String name) {
		this.name = name;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public void setId(int id) {
		this.id = id;
	}


}