package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.DeleteUserLogic;
import model.UserBean;

/**
 * Servlet implementation class UpdateUser
 */
@WebServlet("/DeleteUser")
public class DeleteUser extends HttpServlet implements Servlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DeleteUser() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// フォワード先
		String forwardPath = null;

		// actionの値をリクエストパラメータから取得
		request.setCharacterEncoding("UTF-8");
		String action = request.getParameter("action");

		// 「削除」をリクエストされたときの処理
		if (action.equals("delete")) {
			// リクエストパラメータの取得
			request.setCharacterEncoding("UTF-8");
			int id = Integer.parseInt(request.getParameter("id"));
			String name = request.getParameter("name");
			String country = request.getParameter("country");
			String mail = request.getParameter("mail");
			// 削除するユーザの情報を設定
			UserBean deleteUser = new UserBean(id, name, country, mail);
			// セッションスコープへ保存
			HttpSession session = request.getSession();
			session.setAttribute("deleteUser", deleteUser);
			// データ取得後のフォワード先を設定
			forwardPath = "/WEB-INF/jsp/delete.jsp";
		} else if (action.equals("deleteDone")) {
			// リクエストパラメータの取得
			request.setCharacterEncoding("UTF-8");
			int id = Integer.parseInt(request.getParameter("id"));
			String name = request.getParameter("name");
			String country = request.getParameter("country");
			String mail = request.getParameter("mail");
			// 削除するユーザの情報を設定
			UserBean deleteUser = new UserBean(id, name, country, mail);
			// 削除処理の呼び出し
			DeleteUserLogic logic = new DeleteUserLogic();
			logic.execute(deleteUser);
			// データ取得後のフォワード先を設定
			forwardPath = "/WEB-INF/jsp/deleteDone.jsp";
		}

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher(forwardPath);
		dispatcher.forward(request, response);
	}
}
