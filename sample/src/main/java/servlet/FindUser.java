package servlet;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.GetMemberListLogic;
import model.SearchBean;
import model.SearchLogic;
import model.UserBean;

/**
 * Servlet implementation class FindUser
 */
@WebServlet("/FindUser")
public class FindUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FindUser() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		// フォワード先
		String forwardPath = null;

		// actionの値をリクエストパラメータから取得
		String action = request.getParameter("action");

		if (action == null) {
			// 登録者のリストを取得
			GetMemberListLogic getMemberListLogic = new GetMemberListLogic();
			List<UserBean> memberList = getMemberListLogic.execute();
			// リクエストスコープへ保存
			request.setAttribute("memberList", memberList);
			// データ取得後のフォワード先を設定
			forwardPath = "/WEB-INF/jsp/memberList.jsp";

		}else if (action.equals("back")) {
			// 登録者のリストを取得して
			GetMemberListLogic getMemberListLogic = new GetMemberListLogic();
			List<UserBean> memberList = getMemberListLogic.execute();
			// リクエストスコープへ保存
			request.setAttribute("memberList", memberList);
			// データ取得後のフォワード先を設定
			forwardPath = "/WEB-INF/jsp/memberList.jsp";
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(forwardPath);
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// フォワード先
		String forwardPath = null;

		// actionの値をリクエストパラメータから取得
		request.setCharacterEncoding("UTF-8");
		String action = request.getParameter("action");

		// 「登録の開始」をリクエストされたときの処理
		if (action == null) {

			// フォワード先を設定
			forwardPath = "/WEB-INF/jsp/registerConfirm.jsp";
		}
		// 「検索」をリクエストされたときの処理
		else if (action.equals("find")) {
			request.setCharacterEncoding("UTF-8");
			// リクエストパラメータの取得
			// 検索ボックス内のワードを受け取る
			int japanese = Integer.parseInt(request.getParameter("japanese")); 
		    String searchWord = request.getParameter("searchWord"); 
		    int order = Integer.parseInt(request.getParameter("order"));
		    // 受け取った検索ワードをSearchLogicへ
		    SearchBean SearchUser = new SearchBean(japanese ,searchWord ,order);
		    
		    // DAOにて検索ワードが含まれるDB内の情報を取得
		    List<UserBean> MemberList = SearchLogic.execute(SearchUser);
		    
		    // リクエストスコープへ保存
		    request.setAttribute("MemberList", MemberList);
			// データ取得後のフォワード先を設定
			forwardPath = "/WEB-INF/jsp/seach.jsp";
		}
		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher(forwardPath);
		dispatcher.forward(request, response);
	}

}
