package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class RegisterUser
 */
@WebServlet("/Main")
public class Main extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Main() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
//      // TODO Auto-generated method stub
//      response.getWriter().append("Served at: ").append(request.getContextPath());

		// フォワード先
		String forwardPath = null;

		// actionの値をリクエストパラメータから取得
		request.setCharacterEncoding("UTF-8");
		String action = request.getParameter("action");

		// 「ーーー」をリクエストされたときの処理
		if (action == null) {
			// データ取得後のフォワード先を設定
			forwardPath = "/WEB-INF/jsp/top.jsp";
		}

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher(forwardPath);
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// // TODO Auto-generated method stub
		// doGet(request, response);
		
		// フォワード先
		String forwardPath = null;

		// actionの値をリクエストパラメータから取得
		request.setCharacterEncoding("UTF-8");
		String action = request.getParameter("action");

		// 「ーーー」をリクエストされたときの処理
		if (action == null) {
			// データ取得後のフォワード先を設定
			forwardPath = "/WEB-INF/jsp/top.jsp";
		}

		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher(forwardPath);
		dispatcher.forward(request, response);
	}
}