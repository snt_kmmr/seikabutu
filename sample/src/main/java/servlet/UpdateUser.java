package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.UpdateUserLogic;
import model.UserBean;

/**
 * Servlet implementation class UpdateUser
 */
@WebServlet("/UpdateUser")
public class UpdateUser extends HttpServlet implements Servlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UpdateUser() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// フォワード先
		String forwardPath = null;

		// actionの値をリクエストパラメータから取得
		request.setCharacterEncoding("UTF-8");
		String action = request.getParameter("action");

		// 「変更」をリクエストされたときの処理
		if (action.equals("update")) {
			// リクエストパラメータの取得
			request.setCharacterEncoding("UTF-8");
			int id = Integer.parseInt(request.getParameter("id"));
			String name = request.getParameter("name");
			String country = request.getParameter("country");
			String mail = request.getParameter("mail");
			// 更新するユーザの情報を設定
			UserBean updateUser = new UserBean(id ,name, country, mail);
			// セッションスコープへ保存
			HttpSession session = request.getSession();
			session.setAttribute("updateUser", updateUser);
			// データ取得後のフォワード先を設定
			forwardPath = "/WEB-INF/jsp/update.jsp";
			
		} else if (action.equals("updateDone")) {
			// リクエストパラメータの取得
			request.setCharacterEncoding("UTF-8");
			int id = Integer.parseInt(request.getParameter("id"));
			String name = request.getParameter("name");
			String country = request.getParameter("country");
			String mail = request.getParameter("mail");
			// 更新するユーザの情報を設定
			UserBean updateUser = new UserBean(id, name, country, mail);
			// 更新処理の呼び出し
			UpdateUserLogic logic = new UpdateUserLogic();
			logic.execute(updateUser);
//			// 不要となったセッションスコープ内のインスタンスを削除
//			session.removeAttribute("updateUser");
			// 更新後のフォワード先を設定
			forwardPath = "/WEB-INF/jsp/updateDone.jsp";
		}
		// フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher(forwardPath);
		dispatcher.forward(request, response);
	}
}
