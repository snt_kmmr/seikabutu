<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="model.UserBean"%>
<%UserBean deleteUser = (UserBean) session.getAttribute("deleteUser");%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>削除</title>
<style>
body {
	background-color: #f5f5f5;
}

.block {
	display: block;
	width: 100%;
	font-size: 16px;
	line-height: 1.8;
	margin: 40px auto;
	text-align: center;
}

input {
	box-sizing: border-box;
	margin: 8px 0;
	padding: .8em;
	border-radius: 8px;
	border: 2px solid #aaa;
}

.left {
	text-align: left;
	width: 23%;
	margin: 8px auto;
}

.button {
	display: inline-block;
	border-radius: 4px;
	font-size: 16px;
	text-align: center;
	padding: 8px 12px;
	background: #d3d3d3;
	color: #000000;
	line-height: 1em;
}

.button:hover {
	opacity: 0.8;
}
</style>
</head>

<body>
	<div class="block">
		<h2>-削除-</h2>
		<p>下記のユーザーを削除します。</p>
		<p>
			名前：<%= deleteUser.getName()%><br>国籍：<%= deleteUser.getCountry()%><br> メールアドレス：<%= deleteUser.getMail()%><br>
		</p>
		<form action="/sample/DeleteUser?action=deleteDone" method="post">
			<input type="hidden" name="id" value="<%= deleteUser.getId()%>">
			<input type="hidden" name="name" value="<%= deleteUser.getName()%>">
			<input type="hidden" name="country" value="<%= deleteUser.getCountry()%>">
			<input type="hidden" name="mail" value="<%= deleteUser.getMail()%>">
			<input type="submit" class="button" value="削除">
		</form>
		<br><a href="/sample/FindUser">戻る</a>
	</div>
</body>
</html>