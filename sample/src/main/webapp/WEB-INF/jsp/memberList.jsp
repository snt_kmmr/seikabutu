<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>登録者一覧</title>
<style>
body {
	background-color: #f5f5f5;
}

.memberlist {
	display: -webkit-box;
	display: -ms-flexbox;
	display: flex;
	-webkit-box-align: center;
	-ms-flex-align: center;
	align-items: center;
	-webkit-box-pack: center;
	-ms-flex-pack: center;
	justify-content: center;
	flex-flow: column;
	width: 56%;
	font-size: 16px;
	line-height: 1.6;
	margin: 24px auto;
}

#form5 {
	position: relative;
	margin-bottom: 16px;
	width: auto;
}

input[type=checkbox] {
	width: 24px;
	height: 24px;
	vertical-align: middle;
}

#sbox5 {
	height: 40px;
	padding: 0 16px;
	margin-left: 24px;
	position: relative;
	left: 0;
	top: 0;
	border-radius: 4px;
	background: #ffffff;
	border: 2px solid #aaa;
}

#sbtn5 {
	width: 64px;
	height: 40px;
	margin-left: 24px;
	position: relative;
	top: 0;
	border-radius: 4px;
	background: #ffffff;
	color: #6495ed;
	font-weight: bold;
	font-size: 16px;
	border: 2px solid #6495ed;
}

#sbtn5:hover {
	opacity: 0.4;
}

ul.pagination {
	display: inline-block;
	padding: 0;
	margin-bottom: 16px;
}

ul.pagination li {
	display: inline;
}

ul.pagination li a {
	color: black;
	float: left;
	padding: 8px 16px;
	text-decoration: none;
	transition: background-color .3s;
	background: #ffffff;
}

ul.pagination li a.active {
	background-color: #6495ed;
	color: white;
	border: 1px solid #6495ed;
}

ul.pagination li a:hover:not(.active) {
	background-color: #ddd;
}

table {
	width: 100%;
	border-collapse: separate;
	border-spacing: 0;
	border-radius: 8px;
	overflow: hidden;
	background-color: #ffffff;
	border: 2px solid #aaa;
}

table th, table td {
	padding: 8px 0;
	text-align: center;
}

table tr:nth-child(odd) {
	background-color: #eee
}

form {
	display: inline-block;
}

.red {
	display: inline-block;
	border-radius: 4px;
	font-size: 12pt;
	text-align: center;
	margin: auto 8px;
	padding: 8px 8px;
	background: #dc143c;
	color: #ffffff;
	line-height: 1em;
	border: 1px solid #dc143c;
}

.red:hover {
	opacity: 0.4;
}

.green {
	display: inline-block;
	border-radius: 4px;
	font-size: 12pt;
	text-align: center;
	margin: auto 8px;
	padding: 8px 8px;
	background: #66cdaa;
	color: #ffffff;
	line-height: 1em;
	border: 1px solid #66cdaa;
}

.green:hover {
	opacity: 0.4;
}
</style>
</head>
<body>
	<div class="memberlist">
		<h2>-登録者一覧-</h2>
		<!-- 検索フォーム -->
		<div class="forms">
			<form id="form5" action="/sample/FindUser?action=find" method="post">
				<input type="hidden" name="japanese" value="1"> <input
					type="checkbox" name="japanese" value="2">:日本人のみ <input
					type="text" id="sbox5" id="s" name="serchWord" type="text"
					placeholder="名前を入力" /> <select id="sbox5" id="s" name="order">
					<option value="1">新しい順</option>
					<option value="2">古い順</option>
				</select> <input type="submit" id="sbtn5" type="submit" value="検索" />
			</form>
		</div>
		<!-- 検索フォーム -->

		<!-- 一覧テーブル -->
		<table>
			<tr>
				<th>名前</th>
				<th>国籍</th>
				<th>メールアドレス</th>
				<th>操作</th>
			</tr>

			<c:forEach items="${memberList}" var="membercount">

				<tr>
					<td><c:out value="${membercount.name}" /></td>
					<td><c:out value="${membercount.country}" /></td>
					<td><c:out value="${membercount.mail}" /></td>
					<td>
						<form action="/sample/UpdateUser?action=update" method="post">
							<input type="hidden" name="id" value="${membercount.id}">
							<input type="hidden" name="name" value="${membercount.name}">
							<input type="hidden" name="country"
								value="${membercount.country}"> <input type="hidden"
								name="mail" value="${membercount.mail}"> <input
								type="submit" class="green" value="変更">
						</form>
						<form action="/sample/DeleteUser?action=delete" method="post">
							<input type="hidden" name="id" value="${membercount.id}">
							<input type="hidden" name="name" value="${membercount.name}">
							<input type="hidden" name="country"
								value="${membercount.country}"> <input type="hidden"
								name="mail" value="${membercount.mail}"> <input
								type="submit" class="red" value="削除">
						</form>
					</td>
				</tr>
			</c:forEach>
		</table>
		<!-- 一覧テーブル -->
		
		<!-- ページャー -->
		<ul class="pagination">
			<li><a href="#">«</a></li>
			<li><a class="active" href="#">1</a></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li><a href="#">4</a></li>
			<li><a href="#">5</a></li>
			<li><a href="#">6</a></li>
			<li><a href="#">7</a></li>
			<li><a href="#">8</a></li>
			<li><a href="#">9</a></li>
			<li><a href="#">10</a></li>
			<li><a href="#">»</a></li>
		</ul>
		<!-- ページャー -->
		
		<a href="/sample/Main">戻る</a>
	</div>
	<br>
</body>
</html>