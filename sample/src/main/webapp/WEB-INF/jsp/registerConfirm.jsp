<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="model.UserBean"%>
<%UserBean registerUser = (UserBean) session.getAttribute("registerUser");%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>登録確認</title>
<style>
body {
	background-color: #f5f5f5;
}

.block {
	display: block;
	width: 100%;
	font-size: 16px;
	line-height: 1.8;
	margin: 40px auto;
	text-align: center;
}

.left {
	text-align: left;
	width: 23%;
	margin: 8px auto;
}
</style>
</head>

<body>
	<div class="block">
		<h2>-確認-</h2>
		<p>下記のユーザーを登録します。</p>
		<p>
			名前：<%= registerUser.getName()%><br>
			出身国：<%= registerUser.getCountry()%><br>
			メールアドレス：<%= registerUser.getMail()%><br>
		</p>
		<br> <a href="/sample/RegisterUser">戻る</a> 
		<a href="/sample/RegisterUser?action=done">登録</a>
	</div>
</body>
</html>