<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>登録</title>
<style>
body {
	background-color: #f5f5f5;
}

.block {
	display: block;
	width: 100%;
	font-size: 16px;
	line-height: 1.8;
	margin: 40px auto;
	text-align: center;
}

input {
	box-sizing: border-box;
	margin: 8px 0;
	padding: .8em;
	border-radius: 8px;
	border: 2px solid #aaa;
}

.left {
	text-align: left;
	width: 23%;
	margin: 8px auto;
}

.button {
	display: inline-block;
	border-radius: 4px;
	font-size: 16px;
	text-align: center;
	padding: 8px 12px;
	background: #d3d3d3;
	color: #000000;
	line-height: 1em;
}

.button:hover {
	opacity: 0.8;
}
</style>
</head>

<body>
	<div class="block">
		<h2>-登録-</h2>
		<form action="/sample/RegisterUser" method="post">
				<input type="hidden" name="id" value="0">
			国籍：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="text" name="country" placeholder="例）日本" style="width: 320px;" class="left"><br>
			メールアドレス：<input type="text" name="mail" placeholder="例）test@example.com"
				style="width: 320px;" class="left"><br>
			名前：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="text" name="name" placeholder="例）山田 太郎" style="width: 320px;"class="left"><br>
				<input type="submit" class="button" value="確認">
		</form>
		<br><a href="/sample/Main">戻る</a>
	</div>
</body>
</html>