<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>トップページ</title>
<style>
body {
	background-color: #f5f5f5;
}

.block {
	display: block;
	width: 100%;
	font-size: 16px;
	line-height: 1.8;
	text-align: center;
}

#doughnutChart {
  position: relative;
}

h3 {
   position: absolute;
   text-align: center;
   top: 40%;
   left: 45%;
   -webkit-transform: translateY(-50%);
   transform: translateY(-50%); 
}

</style>
</head>

<body>
	<div class="block">
		<h2>-グラフ-</h2>
		<canvas id="doughnutChart"  height="80"></canvas>
		<h3>国別登録者の割合</h3>
		<script
			src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.js"></script>
		<script>
		  var ctx = document.getElementById("doughnutChart");
		  var myPieChart = new Chart(ctx, {
			type: 'doughnut',
		    data: {
		      labels: ["日本", "アメリカ", "中国", "その他"],
		      datasets: [{
		          backgroundColor: [
		              "#FF759A",
		              "#FFDA75",
		              "#75FFDA",
		              "#759AFF"
		          ],
		          data: [65, 8, 8, 19]
		      }]
		    },
		    options: {
	        layout: {

	          }
		    }
		  });
		</script>
		<br>
		<a href="/sample/FindUser">登録者一覧</a>&nbsp;<a href="/sample/RegisterUser">登録画面</a>
	</div>
</body>
</html>