<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="model.UserBean"%>
<%UserBean updateUser = (UserBean) session.getAttribute("updateUser");%>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>更新</title>
<style>
body {
	background-color: #f5f5f5;
}

.block {
	display: block;
	width: 100%;
	font-size: 16px;
	line-height: 1.8;
	margin: 40px auto;
	text-align: center;
}

input {
	box-sizing: border-box;
	margin: 8px 0;
	padding: .8em;
	border-radius: 8px;
	border: 2px solid #aaa;
}

.left {
	text-align: left;
	width: 23%;
	margin: 8px auto;
}

.button {
	display: inline-block;
	border-radius: 4px;
	font-size: 16px;
	text-align: center;
	padding: 8px 12px;
	background: #d3d3d3;
	color: #000000;
	line-height: 1em;
}

.button:hover {
	opacity: 0.5;
}
</style>
</head>

<body>
	<div class="block">
		<h2>-更新-</h2>
		<p>下記ユーザーの情報を編集し、更新ボタンを押してください。</p>
		<form action="/sample/UpdateUser?action=updateDone" method="post">
				<input type="hidden" name="id" value="<%= updateUser.getId()%>">
			国籍：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="text" name="country" style="width: 320px;" class="left"
				value="<%= updateUser.getCountry()%>"><br>
			メールアドレス：
				<input type="text" name="mail" style="width: 320px;" class="left"
				value="<%= updateUser.getMail()%>"><br>
			名前：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<input type="text" name="name" style="width: 320px;" style="width: 160px;" class="left"
				value="<%= updateUser.getName()%>"><br><br>
				<input type="submit" class="button" value="更新">
		</form>
		<br><a href="/sample/FindUser">戻る</a>
	</div>
</body>
</html>