<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>更新完了</title>
<style>
body {
	background-color: #f5f5f5;
}

.block {
	display: block;
	width: 100%;
	font-size: 16px;
	line-height: 1.8;
	margin: 40px auto;
	text-align: center;
}
</style>

</head>
<body>
	<div class="block">
		<h2>Thank you!!</h2>
		<p>更新が完了しました。</p>
		<br><a href="/sample/FindUser">登録者一覧へ</a>
	</div>
</body>
</html>