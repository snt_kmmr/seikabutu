package app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.servlet.handler.MappedInterceptor;

import app.domain.AuthInterceptor;

@SpringBootApplication
@EnableJpaRepositories("app.repository")
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	//interceptor→リクエストをマッピングする前にアクセスしてきたユーザを認証する処理
	//全範囲("/**")にAuthInterceptorを適用
    @Bean
    public MappedInterceptor interceptor() {
        return new MappedInterceptor(new String[]{"/**"}, AuthInterceptor());
    }
    @Bean
    public AuthInterceptor AuthInterceptor() {
        return new AuthInterceptor();
    }
}
