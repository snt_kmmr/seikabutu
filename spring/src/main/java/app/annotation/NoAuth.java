package app.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

//classファイルに記録され実行時に参照できる(アプリケーション実行時に値を取得)
@Retention(RetentionPolicy.RUNTIME)
//Targetアノテーションを付けることで「何につけられるアノテーションなのか？」を指定(アノテーションを適用する場所)
@Target(ElementType.METHOD)
public @interface NoAuth {

}
