package app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import lombok.RequiredArgsConstructor;

@Controller
@RequiredArgsConstructor
@RequestMapping("/event")
public class EventController {

	@RequestMapping("")
	public String index() {
		return "event/index";
	}
	
	// マイページのリクエスト
	@RequestMapping(value = "home", method = RequestMethod.GET)
	public String home() {
		return "redirect:/home";
	}
	
	// マイページのリクエスト
	@RequestMapping(value = "mypage", method = RequestMethod.GET)
	public String userMypage() {
		return "redirect:/mypage";
	}

	// 新規予定のリクエスト
	@RequestMapping(value = "eventCreate", method = RequestMethod.GET)
	public String eventCreate() {
		return "event/create";
	}

}
