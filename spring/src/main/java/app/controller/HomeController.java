package app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import lombok.RequiredArgsConstructor;

@Controller
@RequiredArgsConstructor
@RequestMapping("/home")
public class HomeController {

	@RequestMapping("")
	public String index() {
		return "home/index";
	}

	// マイページのリクエスト
	@RequestMapping(value = "mypage", method = RequestMethod.GET)
	public String userMypage() {
		return "redirect:/mypage/index";
	}
	

}
