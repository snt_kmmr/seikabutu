package app.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import app.annotation.NoAuth;
import app.domain.AuthUser;
import app.domain.Authority;
import app.domain.User;
import app.repository.AuthorityRepository;
import app.repository.UserRepository;
import app.service.LoginService;
import lombok.RequiredArgsConstructor;

@Controller
@RequiredArgsConstructor
@RequestMapping("/login")
public class LoginController {

	@Autowired
	private AuthUser authUser;
	// ログイン・ログアウトの前後でセッションを張り替えるために用意
	@Autowired
	private HttpSession session;
	@Autowired
	private HttpServletRequest request;
	@Autowired
	LoginService loginService;
	@Autowired
	AuthorityRepository authorityRepository;
	@Autowired
	UserRepository userRepository;
	
	
	@RequestMapping("")
	// @NoAuth→このアノテーションがついている場合は認証しているかチェックする
	@NoAuth
	public String index() {
		System.out.println(authUser.isLoginFlg());
		return "login/index";
	}

	// ログインのリクエスト
	@RequestMapping(value = "doLogin", method = RequestMethod.POST)
	@NoAuth
	public String doLogin(@RequestParam("userName") String userName, @RequestParam("userPass") String userPass,
			@RequestParam("authority") String authority) {
		
		User user = new User();
		user.setUserName(userName);
		user.setUserPass(userPass);
		user.setAuthority(authorityRepository.findByAuthorityCode(authority));
		Authority auth = user.getAuthority();
	
		List<User> LoginUser = userRepository.findByUserNameAndUserPassAndAuthorityId(userName, userPass, auth.getId());
		
		if (LoginUser != null && !LoginUser.isEmpty()) {
			session.invalidate();
			session = request.getSession(true);
			// セッションスコープの authUser に DB から取得した情報をセット(ログイン中のユーザー情報)
			User currentUser = LoginUser.get(0);
			authUser.setUserName(currentUser.getUserName());
			authUser.setUserPass(currentUser.getUserPass());
			authUser.setAuthority(currentUser.getAuthority());
			authUser.setLoginFlg(true);
			
			 return "redirect:../home";
		} else {
			// 認証に失敗したら、ログインエラーページにリダイレクトする
			authUser.setLoginFlg(false);
			return "login/login_error";
		}

	}

	// ログアウトのリクエスト
	@RequestMapping(value = "doLogout", method = RequestMethod.GET)
	@NoAuth
	public String doLogout() {
		session.invalidate();
		session = request.getSession(true);
		authUser.setLoginFlg(false);
		return "redirect:/login";
	}
	
	
}
