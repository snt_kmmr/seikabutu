package app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import lombok.RequiredArgsConstructor;

@Controller
@RequiredArgsConstructor
@RequestMapping("/mypage")
public class MypageController {

	@RequestMapping("")
	public String index() {
		return "mypage/index";
	}

	// aaaのリクエスト
	@RequestMapping(value = "aaa", method = RequestMethod.GET)
	public String userMypage() {
		return "redirect:/aaa/index";
	}

}
