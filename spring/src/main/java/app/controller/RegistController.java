package app.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import app.annotation.NoAuth;
import app.domain.User;
import app.domain.UserRequest;
import app.repository.AuthorityRepository;
import app.repository.UserRepository;
import lombok.RequiredArgsConstructor;

@Controller
@RequiredArgsConstructor
@RequestMapping("/regist")
public class RegistController {
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	AuthorityRepository authorityRepository;

	// ユーザー新規登録画面を表示
	@RequestMapping("")
	@NoAuth
	public String index() {
		return "regist/index";
	}

	// 「新規登録」のリクエスト
	@RequestMapping(value = "create", method = RequestMethod.POST)
	@NoAuth
	public String registUser(@ModelAttribute UserRequest userRequest) {
		User user = new User();
		user.setAuthority(authorityRepository.findByAuthorityName(userRequest.getAuthority()));
		user.setUserName(userRequest.getUserName());
		user.setUserPass(userRequest.getUserPass());
		userRepository.save(user);
		return "regist/done";
	}

}
