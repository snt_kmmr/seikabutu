package app.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import lombok.RequiredArgsConstructor;

@Controller
@RequiredArgsConstructor
@RequestMapping("/setting")
public class SettingController {

	@RequestMapping("")
	public String index() {
		return "mypage/index";
	}
	
	// マイページのリクエスト
	@RequestMapping(value = "home", method = RequestMethod.GET)
	public String home() {
		return "redirect:/home";
	}
	
	// マイページのリクエスト
	@RequestMapping(value = "mypage", method = RequestMethod.GET)
	public String userMypage() {
		return "redirect:/mypage";
	}
	
	// のリクエスト
	@RequestMapping(value = "location", method = RequestMethod.GET)
	public String SettingLocation() {
		return "redirect:setting/location";
	}

}
