package app.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.data.annotation.CreatedBy;

import lombok.Data;

@Entity
@Data
public class Attend {
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    
    private Integer id;
    
    private Integer eventId;
    
    private String symbol;
    
    @CreatedBy
    private String userName;
    
    private String comment;

}
