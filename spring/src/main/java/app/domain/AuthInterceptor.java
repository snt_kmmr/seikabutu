package app.domain;

import java.lang.reflect.Method;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.resource.ResourceHttpRequestHandler;

import app.annotation.NoAuth;


public class AuthInterceptor implements HandlerInterceptor{
	
    @Autowired
    private AuthUser authUser;
    
    //preHandle→Controllerにリクエストが行く前に呼ばれる
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
    	
        //静的リソースの場合は認証不要にする
        if (handler instanceof ResourceHttpRequestHandler) {
              return true;
        }
        //認証済みかチェックする
    	HandlerMethod h = (HandlerMethod) handler;
        Method m = h.getMethod();
        NoAuth n = AnnotationUtils.findAnnotation(m, NoAuth.class);
        if(n != null) {
            return true;
        }
        if(!authUser.isLoginFlg()) {
            //未ログイン状態である場合ログイン画面に飛ばす
            response.sendRedirect("/login/");
            return false;
        }
        return authUser.isLoginFlg();
    }

}
