package app.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;

@Data
@EntityListeners(AuditingEntityListener.class)
public class Event implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)

	private Integer id;

	private LocalDate eventDate;

	private LocalTime eventTime;

	@ManyToOne
	@JoinColumn(name = "location_id")
	private List<Location> locationList;

	@ManyToOne
	@JoinColumn(name = "category_id")
	private List<Category> categoryList;

	@ManyToOne
	@JoinColumn(name = "waiting_id")
	private List<Waiting> waitingList;

	private String other;

	@CreatedDate
	private LocalDate createDate = LocalDate.now();

	@CreatedBy
	@ManyToOne
	@JoinColumn(name = "user_id")
	private int userId;

	@CreatedBy
	private int userName;
}