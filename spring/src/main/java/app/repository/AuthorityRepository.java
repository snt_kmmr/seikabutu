package app.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import app.domain.Authority;

@Repository
public interface AuthorityRepository extends JpaRepository<Authority, Integer> {

	public Authority findByAuthorityName(String authority);
	
	public Authority findByAuthorityCode(String authority);
	
}
