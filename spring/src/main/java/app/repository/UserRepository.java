package app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import app.domain.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
	
	public List<User> findByUserNameAndUserPassAndAuthorityId(String userName, String userPass, int authorityId);

}
