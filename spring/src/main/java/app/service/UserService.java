package app.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import app.domain.User;
import app.repository.AuthorityRepository;
import app.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	AuthorityRepository userCreateRepository;

	// ユーザー情報 全検索

	public List<User> searchAll() {
		return userRepository.findAll();
	}

}
