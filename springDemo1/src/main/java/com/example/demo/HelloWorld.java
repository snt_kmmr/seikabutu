package com.example.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HelloWorld {
	@GetMapping("/") // このClassがユーザからのアクセスを受け取ることができる
	public String hello() {
		return "hello"; // hello.htmlをユーザに返す
	}
}
