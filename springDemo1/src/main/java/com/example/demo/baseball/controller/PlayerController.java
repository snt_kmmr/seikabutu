package com.example.demo.baseball.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.baseball.domain.Player;
import com.example.demo.baseball.service.PlayerService;

//クラスに対してRequestMappingを付けておくと、クラス内のメソッド全てに適用される
//つまりこのクラスのメソッドは全て、http://localhost:8081/playersから始まるURLにマッピングされている
@Controller
@RequestMapping("/players")
public class PlayerController {
	@Autowired
	private PlayerService playerService;

	// ②メソッドの引数にModel型の値を設定するとModelのインスタンスが自動的に渡される
	@GetMapping
	public String index(Model model) {
		List<Player> players = playerService.findAll();
		// 受け取ったmodelに値を詰めることで、テンプレートに値を渡すことができる（ここではplayersというキー名でplayerのListを設定している）
		model.addAttribute("players", players);
		// returnしている文字列を元に、src/main/resources/templates/配下からファイルを見つけてユーザに返している
		return "players/index";
	}

	@GetMapping("new")
	public String newPlayer(Model model) {
		Player player = new Player();
		model.addAttribute("player", player);
		return "players/new";
	}

	@GetMapping("{id}/edit")
	// メソッドの引数に@PathVariableを設定するとURL上の値を取得することができる
	// ここでは、http://localhost/players/1にアクセスされるとidには1が入る
	public String edit(@PathVariable Long id, Model model) {
		Player player = playerService.findOne(id);
		model.addAttribute("player", player);
		return "players/edit";
	}

	@GetMapping("{id}")
	public String show(@PathVariable Long id, Model model) {
		Player player = playerService.findOne(id);
		model.addAttribute("player", player);
		return "players/show";
	}

	@PostMapping
	// メソッドの引数に@ModelAttributeをつけると送信されたリクエストのbodyの情報を取得できる
	public String create(@Valid @ModelAttribute Player player, BindingResult bindingResult) {
		if(bindingResult.hasErrors()) return "players/new";
		playerService.save(player);
		// redirect:/players"とすると/playersにリダイレクトされる
		// createメソッドの処理が終わった後にhttp://localhost:8081/playersに勝手にアクセスされる感じ
		return "redirect:/players";
	}

	@PutMapping("{id}")
	public String update(@PathVariable Long id, @Valid @ModelAttribute Player player, BindingResult bindingResult) {
        if(bindingResult.hasErrors()) return "players/edit";
        player.setId(id);
        playerService.save(player);
        return "redirect:/players";
    }

	@DeleteMapping("{id}")
	public String destroy(@PathVariable Long id) {
		playerService.delete(id);
		return "redirect:/players";
	}
}
