package com.example.demo.baseball.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.baseball.domain.Player;
import com.example.demo.baseball.repository.PlayerRepository;

@Service
public class PlayerService {
	// Autowiredを付けて宣言するとBeanをインジェクトしてくれるのでnewしなくても使える
	@Autowired
	private PlayerRepository playerRepository;

	public List<Player> findAll() {
		return playerRepository.findAll();
	}

	public Player findOne(Long id) {
		// Optionalを返すようになったので取得できなかった場合の処理`.orElse(null)`を追加
		return playerRepository.findById(id).orElse(null);
	}

	public Player save(Player player) {
		return playerRepository.save(player);
	}

	public void delete(Long id) {
		playerRepository.deleteById(id);
	}
}
